const express = require('express');
const app = express();

app.use(express.json())

app.get('/api/article', (req, res) => {
    article.findAll().then(article => {
        res.status(200).json("OK")
    })
});

app.post('/api/article', (req, res) => {

    const { title, body, approved } = req.body
    if (title === undefined || body === undefined || approved === undefined) {
        res.status(400).json("Beberapa field kosong")
        return
    }

    article.create({
        title: req.body.title,
        bpdy: req.body.body,
        approved: req.body.approved
    }).then(article => {
        res.status(200).json("OK")
    }).catch(err => {
        res.status(500).json("Internet Server Eror")
    })
});

app.put('/api/article.:id', (req, res) => {
    const { title, body, approved } = req.body
    if (title === undefined || body === undefined || approved === undefined) {
        return res.status(400).json("Beberapa field kosong")
    }

    const whereClause = {
        where: { id: req.params.id }
    }

    article.update({
        title: title,
        body: body,
        approved: approved
    }, whereClause).then(() => {
        res.status(200).json("OK")
        process.exit()
    })
        .catch(err => {
            console.error("Gagal mengupdate artikel")
        })
});

app.delete('/api/article/:id', (req, res) => {
    article.destroy({
        where: { id: req.params.id }
    }).then(() => {
        res.status(200).json("OK")
    })
});

app.listen(3000, () => {
    console.log(`Server started on 3000`);
});