const express = require('express')
const app = express()


const articleRouter = require("./router/articleRouters")

app.use(express.json())

app.use(express.urlencoded({
    extended: false,
}))

// Set view engine => EJS
app.set('view engine', 'ejs')
app.use(articleRouter);



app.listen(3000, () => {
    console.log(`Server started on port`);
});


// Sub domain (binaracademy.com)
// API => api.binaracademy.com
// UI => binaracademy.com

// binaracademy.com/article

// Route ke API
// Route ke View Engine



// DELETE TABLE_NAME WHERE ID = 50
// UPDATE TABLE_NAME SET nama = 'dezan' WHEREE ID = 23



// app.get('/article', (req, res) => {
//     Article.findAll().then(article => {
//         res.status(200).json(article)
//     })
// });

// app.put('/api/article/:id', (req, res) => {
//     const {title, body, approved} = req.body

//     if (title === undefined || body === undefined || approved === undefined){
//         return res.status(400).json("Beberapa field kosong!")
//     }

//     const whereClause = {
//         where: { id: req.params.id }
//     }

//     Article.update({
//         title: title,
//         body: body,
//         approved: approved  
//     }, whereClause).then(() => {
//         res.status(200).json("OK")
//         process.exit()
//       })
//       .catch(err => {
//         console.error("Gagal mengupdate artikel! ", err)
//       })
// });

// app.delete('/api/article/:id', (req, res) => {
//     Article.destroy({
//         where : {id : req.params.id}
//     }).then(() => {
//         res.status(200).json("OK")
//     })

// });