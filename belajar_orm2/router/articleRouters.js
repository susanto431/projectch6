const express = require('express');
const articleRouter = express()
const { article } = require("../models")

articleRouter.get('/article/create', (req, res) => {
    res.render('articles/create')
})

// Proses input
articleRouter.post('/api/article', (req, res) => {
    // Validasi!
    const { title, body } = req.body

    if (title === undefined || body === undefined) {
        return res.status(400).json("Beberapa field kosong!")
    }

    Article.create({
        title: title,
        body: body,
        approved: false
    }).then(article => {
        res.status(200).json("Berhasil membuat artikel!")
    }).catch(err => {
        console.log(err)
        res.status(500).json("Internal Server Error")
    })
})
